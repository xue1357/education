package com.swu.education.util;

/**
 * Created by nana on 2018/1/2.
 */

import com.iflytek.cloud.speech.*;

import java.util.ArrayList;
import java.util.List;

public class VoiceToWord {

  int sessionStatus = 1;
  List<String> responseMes = new ArrayList<>();

  public void speak() {

    StringBuffer param = new StringBuffer();
    param.append( "appid=" + Version.getAppid() );
    SpeechUtility.createUtility( param.toString() );

    SpeechRecognizer mIat= SpeechRecognizer.createRecognizer( );
    //2.设置听写参数，详见《MSC Reference Manual》SpeechConstant类
    mIat.setParameter(SpeechConstant.DOMAIN, "iat");
    mIat.setParameter(SpeechConstant.LANGUAGE, "zh_cn");
    mIat.setParameter(SpeechConstant.ACCENT, "mandarin ");
    mIat.setParameter(SpeechConstant.KEY_SPEECH_TIMEOUT,"60000");

    //听写监听器
    RecognizerListener mRecoListener = new RecognizerListener(){
      //听写结果回调接口(返回Json格式结果，用户可参见附录)；
      //一般情况下会通过onResults接口多次返回结果，完整的识别内容是多次结果的累加；
      //关于解析Json的代码可参见MscDemo中JsonParser类；
      //isLast等于true时会话结束。
      public void onResult(RecognizerResult results, boolean isLast){

        String text = results.getResultString();
        // 将结果存入list
        responseMes.add(text);

        if(sessionStatus==1){
          isLast = true;
        }
        if (isLast)  //给isLast赋值
        {
          System.out.println("会话结束=====打印并且赋值最终结果");
          setListResult(responseMes);
        }
      }
      //会话发生错误回调接口
      public void onError(SpeechError error) {
        System.out.println( "error: "+error.getErrorCode() );//获取错误码描述
      }
      //开始录音
      public void onBeginOfSpeech() {
        System.out.println("begin");
      }
      //音量值0~30
      public void onVolumeChanged(int volume){}
      //结束录音
      public void onEndOfSpeech() {
        System.out.println("end");
      }
      //扩展用接口
      public void onEvent(int eventType,int arg1,int arg2,String msg) {}
    };
    //3.开始听写
    mIat.startListening(mRecoListener);

  }
  public void stopSession(){
    sessionStatus = 0;
  }

  public List<String> getListResult() {
    return responseMes;
  }

  public void setListResult(List responseMes) {
    this.responseMes = responseMes;
  }

}
