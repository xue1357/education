package com.swu.education.util;

import com.iflytek.cloud.speech.*;
import org.springframework.stereotype.Component;

import java.awt.event.ActionListener;

/**
 * Created by nana on 2017/12/24.
 */

public class WordToSpeech {

  public static SynthesizerListener synthesizeListener;

  public void start(String mText) {

    System.out.println(mText);
    StringBuffer param = new StringBuffer();
    param.append( "appid=" + Version.getAppid() );
    SpeechUtility.createUtility( param.toString() );

    SpeechSynthesizer mTts= SpeechSynthesizer.createSynthesizer( );

    mTts.setParameter(SpeechConstant.VOICE_NAME, "小燕");
    mTts.setParameter(SpeechConstant.BACKGROUND_SOUND,"0");
    mTts.setParameter(SpeechConstant.SPEED, "50");
    mTts.setParameter(SpeechConstant.PITCH, "50");
    mTts.setParameter(SpeechConstant.VOLUME, "50");

    synthesizeListener = new SynthesizerListener() {

      @Override
      public void onBufferProgress(int arg0, int arg1, int arg2,
                                   String arg3) {
        // TODO Auto-generated method stub

      }

      @Override
      public void onCompleted(SpeechError error) {
        // TODO Auto-generated method stub
        System.out.println( "onCompleted enter: "+error );
        if( null != error ){
          System.out.println( "error: "+error.getErrorCode() );
        }
        System.out.println( "onCompleted leave" );
      }

      @Override
      public void onSpeakBegin() {
        // TODO Auto-generated method stub

      }

      @Override
      public void onSpeakPaused() {
        // TODO Auto-generated method stub

      }

      @Override
      public void onSpeakProgress(int arg0, int arg1, int arg2) {
        // TODO Auto-generated method stub

      }

      @Override
      public void onSpeakResumed() {
        // TODO Auto-generated method stub

      }

      public void onEvent(int eventType, int arg1, int arg2, int arg3, Object obj1, Object obj2) {

      }
    };
    mTts.startSpeaking( mText, synthesizeListener );
  }

}
