package com.swu.education.controller;

import bitoflife.chatterbean.AliceBot;
import com.swu.education.service.AliceBotMother;
import org.springframework.web.bind.annotation.RestController;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.swu.education.util.*;

import java.util.List;

/**
 * Created by nana on 2017/12/24.
 */

@RestController
public class demoController {

  private String userMessage = "begin";
  public static final String END = "bye";
  private int status = 1;
  public String response = "";
  public int idCount = 100;


  @RequestMapping(value = "/jump")
  public void match() throws Exception {

    System.out.println("this is a new begin");

    idCount++;
    userMessage = "begin";
    status = 1;

    AliceBotMother mother = new AliceBotMother();
    mother.setUp();
    AliceBot bot = mother.newInstance();
    /*System.err.println("Alice>" + bot.respond("index"));*/
/*    WordToSpeech firstSpeech = new WordToSpeech();
    firstSpeech.sta9rt(bot.respond(userMessage));*/
     while(true)
      {
        if(END.equalsIgnoreCase(userMessage))
          break;
        if(status == 1){
          System.out.println("userMes: "+userMessage);
          response = bot.respond(userMessage);
          System.out.println(idCount + ": " + response);
/*          WordToSpeech wordToSpeech = new WordToSpeech();
          wordToSpeech.start(response);*/
          status = 0;
        }
      }
  }

/*
  @RequestMapping(value = "/onlyWordToSpeech")
  public void onlyWordToSpeech(@RequestParam(value="theWordToSpeech") String theWordToSpeech) {
    WordToSpeech onlyWordToSpeech = new WordToSpeech();
    onlyWordToSpeech.start(theWordToSpeech);
  }
*/

  @RequestMapping(value = "/UserMes")
  public String  getUserMes(@RequestParam(value="userMes") String userMes) {
    userMessage = userMes;
    status = 1;
    try
    {
      Thread.sleep(1000);
    } catch (InterruptedException e)
    {
      e.printStackTrace();
    }
    return response;
  }

  VoiceToWord voiceToWord = new VoiceToWord();

  @RequestMapping(value = "startVoiceToWord")
  public void startVoiceToWord(){
    System.out.println("开始执行");
    VoiceToWord voiceToWordTemp = new VoiceToWord();
    voiceToWord = voiceToWordTemp;
    voiceToWord.speak();
  }

  @RequestMapping(value = "endVoiceToWord")
  public List<String> endVoiceToWord(){

    System.out.println("触发结束录音按钮");
    voiceToWord.stopSession();
    try
    {
      Thread.sleep(2000);
    } catch (InterruptedException e)
    {
      e.printStackTrace();
    }
    List result = voiceToWord.getListResult();
    System.out.println("返回值" + result);

    return result;
  }

}
