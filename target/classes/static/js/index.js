/*=========  登录表单提交检测  ========*/


 var commonUrl = 'http://localhost';
//var commonUrl = 'http://codeyourlife.cn';

var oUserMes = document.getElementById("answerText");
var oUserResponse_btn = document.getElementById("userResponse_btn");

var oSpeakBtn = document.getElementById("speakBtn");
var oStopBtn = document.getElementById("stopBtn");


//获取div对象
var oShowDialog = document.getElementById("showDialog");

var OBeginDialog_btn = document.getElementById("start");

$("body").keydown(function() {
    if (event.keyCode == "13") {//keyCode=13是回车键
        console.log("触发");
        oUserResponse_btn.onclick();
    }
});


OBeginDialog_btn.onclick = function () {

    $("#start").hide();
    $("#answerText").attr("disabled",false);
    $(".teacher_content").css("display","block");

    var teacherTextNode = document.createElement("p");
    teacherTextNode.innerHTML = "老师：你好，很高兴见到你";
    oShowDialog.appendChild(teacherTextNode);

    $.ajax({
        url: commonUrl + ':81/jump',
        success: function () {
            console.log("success");
        },
        error: function () {
            console.log("fail");
        }
    });
}

var commonWord = "而且你能坚持完成整个协作学习的过程，已经很不错了，你应该对自己有信心，相信自己可以做到。再下一次协作学习的时候，你可以尝试一下在小组讨论中积极发言。";

oUserResponse_btn.onclick = function () {

    var userTextNode = document.createElement("p");
    userTextNode.innerHTML = "学生：" + oUserMes.value;
    //给div添加文本元素
    oShowDialog.appendChild(userTextNode);

    oShowDialog.scrollTop = oShowDialog.scrollHeight;
    $.ajax({
        type: 'post',
        url: commonUrl + ':81/UserMes',
        data: {userMes: oUserMes.value},
        success: function (data) {
            var robotTextNode = document.createElement("p");
            robotTextNode.innerHTML = "老师：" + data;
            //给div添加文本元素
            oShowDialog.appendChild(robotTextNode);
            oShowDialog.scrollTop = oShowDialog.scrollHeight;
        },
        error: function () {
            console.log("fail");
        }
    });
}
oSpeakBtn.onclick = function () {
    $.ajax({
        url: commonUrl + ':81/startVoiceToWord',
        success: function () {
            console.log("successStart");
        },
        error: function () {
            console.log("fail");
        }
    });
}
oStopBtn.onclick = function () {
    $.ajax({
        url: commonUrl + ':81/endVoiceToWord',
        success: function (data) {
            console.log("data: ");
            console.log(data[0]);
            var responseVal = "";
            for(var i=0;i<data.length-1;i++){
                var tmpStr = data[i];
                tmpStr = eval("(" + tmpStr + ")");
                var wsListStr = tmpStr.ws;
                for(var j=0;j<wsListStr.length;j++){
                    var cwListStr = wsListStr[j].cw;
                    responseVal = responseVal + cwListStr[0].w;
                }
            }
            console.log(responseVal);
            oUserMes.value = responseVal;
        },
        error: function () {
            console.log("fail");
        }
    });
}
